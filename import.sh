mkdir house nordoc stead room addrob
mv house*.dbf house/
mv nordoc*.dbf nordoc/
mv stead*.dbf stead/
mv room*.dbf room/
mv addrob*.dbf addrob/

# pgdbf -x addrob addrob01.dbf | iconv -f cp866 -t utf-8 | psql fias -p 5433 -U postgres
# echo "First addrob imported"
# for file in addrob*.dbf; do
#     if ! [ $file == "addrob01.dbf" ]; then
#         echo "Importing $file..."
#         pgdbf -C -D -x addrob $file | iconv -f cp866 -t utf-8 | psql fias -p 5433 -U postgres
#     fi
# done

for file in *.dbf; do
    echo "Importing $file..."
    pgdbf $file | iconv -f cp866 -t utf-8 | psql fias -p 5433 -U postgres
done

# for file in *.dbf; do
#     echo "Importing $file..."
#     pgdbf -C -D $file | iconv -f cp866 -t utf-8 | psql fias -p 5433 -U postgres
# done
