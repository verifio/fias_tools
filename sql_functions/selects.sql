-- вывести полный адрес
WITH RECURSIVE child_to_parents AS (
SELECT addrob.* FROM addrob
    WHERE aoid = 'cefe387d-f36f-4752-9404-603c9bb1f0b0'
UNION ALL
SELECT addrob.* FROM addrob, child_to_parents
    WHERE addrob.aoguid = child_to_parents.parentguid
        AND addrob.currstatus = 0
)
SELECT * FROM child_to_parents ORDER BY aolevel;

-- поиск по части адреса
SELECT * FROM addrob WHERE formalname ILIKE '%Ульян%';

SELECT
    ao.shortname
    ,ao.formalname as obj
    ,cast(aolevel as char)||ao.shortname as xx
FROM
    addrob ao
WHERE
    livestatus='1'
    and  actstatus='1'
    AND formalname ilike '%содовая%'
    ORDER BY  ao.aolevel,formalname limit 10;

https://explain.depesz.com/                