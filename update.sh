#!/bin/bash
PATHTO=/srv/fias
TOOLS=$PATHTO/fias_tools
FILE=fias_dbf.rar
DBFS=dbfs
LOG=$PATHTO/update_db.log

echo "" >> $LOG
echo "========================= $(date '+%F %T') UPDATE START =========================" >> $LOG
rm $PATHTO/$FILE 2>/dev/null

echo "$(date '+%T') Start downloading archive..." | tee -a $LOG

aria2c -x8 -d $PATHTO -o $FILE -l - --log-level notice http://fias.nalog.ru/Public/Downloads/Actual/fias_dbf.rar >> $LOG 2>&1
if [ $(echo $?) != 0 ]; then
 echo "$(date '+%T') Download FAIL" | tee -a $LOG
 exit
else
 echo "$(date '+%T') Download DONE" | tee -a $LOG
fi

echo "$(date '+%T') Start extracting" | tee -a $LOG
7z x $PATHTO/$FILE -o$PATHTO/$DBFS '-xr!HOUSE*.DBF' '-xr!NORDOC*.DBF' '-xr!ROOM*.DBF' '-xr!STEAD*.DBF' >> $LOG 2>&1
echo "$(date '+%T') Extract complete" | tee -a $LOG

echo "$(date '+%T') Renaming..."  | tee -a $LOG
for f in $PATHTO/$DBFS/*; do mv "$f" "$f.tmp"; mv "$f.tmp" "`echo $f | tr "[:upper:]" "[:lower:]"`"; done
echo "$(date '+%T') Renaming complete." | tee -a $LOG

echo "$(date '+%T') Create dir structure" | tee -a $LOG
mkdir $PATHTO/$DBFS/addrob
mv $PATHTO/$DBFS/addrob*.dbf $PATHTO/$DBFS/addrob/
echo "$(date '+%T') Dir structure created" | tee -a $LOG

export PGPASSWORD="postgres"
echo "$(date '+%T') Drop old tables" | tee -a $LOG
SQL_DROP="DROP TABLE IF EXISTS actstat; \
      DROP TABLE IF EXISTS addrob; \
      DROP TABLE IF EXISTS addrobindex; \
      DROP TABLE IF EXISTS centerst; \
      DROP TABLE IF EXISTS curentst; \
      DROP TABLE IF EXISTS daddrob; \
      DROP TABLE IF EXISTS dhouse; \
      DROP TABLE IF EXISTS eststat; \
      DROP TABLE IF EXISTS flattype; \
      DROP TABLE IF EXISTS hststat; \
      DROP TABLE IF EXISTS intvstat; \
      DROP TABLE IF EXISTS ndoctype; \
      DROP TABLE IF EXISTS opertype; \
      DROP TABLE IF EXISTS socrbase; \
      DROP TABLE IF EXISTS strstat;"

echo $SQL_DROP | psql fias -U postgres >> $LOG 2>&1

echo "$(date '+%T') Start import dbf" | tee -a $LOG

pgdbf -x addrob $PATHTO/$DBFS/addrob/addrob01.dbf | iconv -f cp866 -t utf-8 | psql fias -p 5432 -U postgres >> $LOG 2>&1
echo "$(date '+%T')First addrob imported"  | tee -a $LOG
for file in $PATHTO/$DBFS/addrob/addrob*.dbf; do
   if ! [ $file == "$PATHTO/$DBFS/addrob/addrob01.dbf" ]; then
       echo "$(date '+%T') Importing $file..." | tee -a $LOG
       pgdbf -C -D -x addrob $file | iconv -f cp866 -t utf-8 | psql fias -p 5432 -U postgres >> $LOG 2>&1
   fi
done

for file in $PATHTO/$DBFS/*.dbf; do
   echo "Importing $file..." | tee -a $LOG
   pgdbf $file | iconv -f cp866 -t utf-8 | psql fias -p 5432 -U postgres >> $LOG 2>&1
done

echo "$(date '+%T') Fixing database..." | tee -a $LOG
./fix.py fix-fias.json | psql fias -p 5432 -U postgres >> $LOG 2>&1
echo "$(date '+%T') Database fixing complete" | tee -a $LOG

echo "$(date '+%T') Create indexes" | tee -a $LOG
if [ "$(psql -U postgres -d fias --set ON_ERROR_STOP=on -f $TOOLS/perform_sql/indexes.sql 2>>$LOG | grep COMMIT)" != "" ]; then
  echo "$(date '+%T') Creating indexes complete" | tee -a $LOG
else
  PGPASSWORD=""
  echo "$(date '+%T') Creating indexes FAIL" | tee -a $LOG
  exit
fi

echo "$(date '+%T') Create search database" | tee -a $LOG
if [ "$(psql -U postgres -d fias --set ON_ERROR_STOP=on -f $TOOLS/perform_sql/addrob_index.sql 2>>$LOG | grep COMMIT)" != "" ]; then
  echo "$(date '+%T') Creating search database complete" | tee -a $LOG
else
  PGPASSWORD=""
  echo "$(date '+%T') Creating search database FAIL" | tee -a $LOG
  exit
fi

# echo "Rename to addrobindex_current"

# SQL_RENAME="BEGIN; DROP TABLE IF EXISTS addrobindex_current; \
#   ALTER TABLE addrobindex RENAME TO addrobindex_current; COMMIT;"

# echo $SQL_RENAME | psql fias -U postgres >> $LOG 2>&1

PGPASSWORD=""
echo "$(date '+%F %T') UPDATE DONE!" | tee -a $LOG
