BEGIN;

DELETE FROM addrob WHERE livestatus = 0;

ALTER TABLE public.addrob
  ADD CONSTRAINT addrob_pkey PRIMARY KEY(aoguid);

CREATE UNIQUE INDEX aoguid_pk_idx
  ON public.addrob
  USING btree
  (aoguid);

ALTER TABLE addrob
  ADD CONSTRAINT addrob_parentguid_fkey FOREIGN KEY (parentguid)
  REFERENCES addrob (aoguid) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE public.addrob
  ADD CONSTRAINT addrob_aoid_key UNIQUE(aoid);

CREATE UNIQUE INDEX aoid_idx ON addrob USING btree (aoid);

CREATE INDEX addrob_aolevel
  ON public.addrob
  USING btree
  (aolevel);

CREATE INDEX addrob_parentguid
  ON public.addrob
  USING btree
  (parentguid);

CREATE INDEX addrob_aolevel_shortname_livestatus
  ON public.addrob
  USING btree
  (aolevel, shortname COLLATE pg_catalog."default", livestatus);

CREATE INDEX addrob_currstatus_id_uniq
  ON public.addrob
  USING btree
  (currstatus);

CREATE INDEX addrob_shortname
  ON public.addrob
  USING btree
  (shortname COLLATE pg_catalog."default");

CREATE INDEX addrob_formalname
  ON public.addrob
  USING btree
  (formalname COLLATE pg_catalog."default");

CREATE INDEX addrob_formalname_chars
  ON public.addrob
  USING btree
  (formalname COLLATE pg_catalog."default" varchar_pattern_ops);

CREATE INDEX addrob_shortname_char
  ON public.addrob
  USING btree
  (shortname COLLATE pg_catalog."default" varchar_pattern_ops);

CREATE INDEX addrob_shortname_formalname
  ON public.addrob
  USING btree
  (shortname COLLATE pg_catalog."default", formalname COLLATE pg_catalog."default");

COMMIT;
