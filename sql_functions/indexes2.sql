-- extension to implement trigrams;
CREATE EXTENSION pg_trgm;


-- drop irrelevant data
DELETE FROM addrob WHERE livestatus != 1 AND currstatus != 0;


--========== SOCRBASE ==========--

-- primary key (shortname)
-- ALTER TABLE socrbase DROP CONSTRAINT socrbase_pkey;
ALTER TABLE socrbase ADD CONSTRAINT socrbase_pkey PRIMARY KEY(kod_t_st);

CREATE UNIQUE INDEX kod_t_st_idx ON socrbase USING btree (kod_t_st);
CREATE INDEX scname_level_idx ON socrbase USING btree (scname, level);


--========== addrob ==========--


-- primary key (aoguid)
-- ALTER TABLE addrob DROP CONSTRAINT addrob_pkey;
ALTER TABLE addrob ADD CONSTRAINT addrob_pkey PRIMARY KEY(aoguid);


-- foreign key (parentguid to aoguid)
-- ALTER TABLE addrob DROP CONSTRAINT addrob_parentguid_fkey;
ALTER TABLE addrob
  ADD CONSTRAINT addrob_parentguid_fkey FOREIGN KEY (parentguid)
  REFERENCES addrob (aoguid) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE NO ACTION;


--  create btree indexes
CREATE UNIQUE INDEX aoguid_pk_idx ON addrob USING btree (aoguid);
CREATE UNIQUE INDEX aoid_idx ON addrob USING btree (aoid);
CREATE INDEX parentguid_idx ON addrob USING btree (parentguid);
CREATE INDEX currstatus_idx ON addrob USING btree (currstatus);
CREATE INDEX aolevel_idx ON addrob USING btree (aolevel);
CREATE INDEX formalname_idx ON addrob USING btree (formalname);
CREATE INDEX offname_idx ON addrob USING btree (offname);
CREATE INDEX shortname_idx ON addrob USING btree (shortname);
CREATE INDEX shortname_aolevel_idx ON addrob USING btree (shortname, aolevel);


-- trigram indexes to speed up text searches
CREATE INDEX formalname_trgm_idx on addrob USING gin (formalname gin_trgm_ops);
CREATE INDEX offname_trgm_idx on addrob USING gin (offname gin_trgm_ops);