##
ALTER TABLE public.addrob
  ADD CONSTRAINT addrob_pkey PRIMARY KEY(aoguid);

ALTER TABLE addrob ADD CONSTRAINT addrob_pkey PRIMARY KEY(aoguid);

##
-- foreign key (parentguid to aoguid)
-- ALTER TABLE addrob DROP CONSTRAINT addrob_parentguid_fkey;
ALTER TABLE addrob
  ADD CONSTRAINT addrob_parentguid_fkey FOREIGN KEY (parentguid)
  REFERENCES addrob (aoguid) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE NO ACTION;

##
ALTER TABLE public.addrob
  ADD CONSTRAINT addrob_aoid_key UNIQUE(aoid);

##
ALTER TABLE public.addrob
  ADD CONSTRAINT addrob_aolevel_check CHECK (aolevel >= 0);

ALTER TABLE public.addrob
  ADD CONSTRAINT addrob_centstatus_id_b39cccfa_fk_fias_centerst_centerstid FOREIGN KEY (centstatus_id)
      REFERENCES public.fias_centerst (centerstid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE public.addrob
  ADD CONSTRAINT addrob_currstatus_id_bd50ef0d_fk_fias_curentst_curentstid FOREIGN KEY (currstatus_id)
      REFERENCES public.fias_curentst (curentstid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE public.addrob
  ADD CONSTRAINT addrob_operstatus_id_4275745e_fk_fias_operstat_operstatid FOREIGN KEY (operstatus_id)
      REFERENCES public.fias_operstat (operstatid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;



-- =====

CREATE UNIQUE INDEX aoguid_pk_idx
  ON public.addrob
  USING btree
  (aoguid);

CREATE INDEX addrob_aolevel
  ON public.addrob
  USING btree
  (aolevel);

CREATE INDEX addrob_parentguid
  ON public.addrob
  USING btree
  (parentguid);

CREATE INDEX addrob_aolevel_shortname_livestatus
  ON public.addrob
  USING btree
  (aolevel, shortname COLLATE pg_catalog."default", livestatus);

CREATE INDEX addrob_centstatus_id_uniq
  ON public.addrob
  USING btree
  (centstatus);

CREATE INDEX addrob_currstatus_id_uniq
  ON public.addrob
  USING btree
  (currstatus);

CREATE INDEX addrob_shortname
  ON public.addrob
  USING btree
  (shortname COLLATE pg_catalog."default");

CREATE INDEX addrob_formalname
  ON public.addrob
  USING btree
  (formalname COLLATE pg_catalog."default");

CREATE INDEX addrob_formalname_chars
  ON public.addrob
  USING btree
  (formalname COLLATE pg_catalog."default" varchar_pattern_ops);

CREATE INDEX addrob_operstatus_id_uniq
  ON public.addrob
  USING btree
  (operstatus_id);

CREATE INDEX addrob_shortname_char
  ON public.addrob
  USING btree
  (shortname COLLATE pg_catalog."default" varchar_pattern_ops);

CREATE INDEX addrob_shortname_formalname
  ON public.addrob
  USING btree
  (shortname COLLATE pg_catalog."default", formalname COLLATE pg_catalog."default");



  -- ======

  CREATE TABLE public.addrobindex
(
  id integer NOT NULL DEFAULT nextval('addrobindex_id_seq'::regclass),
  aoguid uuid NOT NULL,
  aolevel smallint NOT NULL,
  scname text NOT NULL,
  fullname text NOT NULL,
  item_weight smallint NOT NULL,
  search_vector tsvector,
  CONSTRAINT addrobindex_pkey PRIMARY KEY (id),
  CONSTRAINT addrobindex_aolevel_check CHECK (aolevel >= 0),
  CONSTRAINT addrobindex_item_weight_check CHECK (item_weight >= 0)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.addrobindex
  OWNER TO postgres;

-- Index: public.addrobindex_aolevel_e8a04882_uniq

-- DROP INDEX public.addrobindex_aolevel_e8a04882_uniq;

CREATE INDEX addrobindex_aolevel_e8a04882_uniq
  ON public.addrobindex
  USING btree
  (aolevel);

-- Index: public.addrobindex_item_weight_96602e94_uniq

-- DROP INDEX public.addrobindex_item_weight_96602e94_uniq;

CREATE INDEX addrobindex_item_weight_96602e94_uniq
  ON public.addrobindex
  USING btree
  (item_weight);
