BEGIN;

ALTER TABLE socrbase ADD COLUMN item_weight smallint NOT NULL DEFAULT 64;

ALTER TABLE public.socrbase
  ADD CONSTRAINT socrbase_pkey PRIMARY KEY(kod_t_st);

UPDATE socrbase SET item_weight = 128 WHERE scname = 'г' OR scname = 'г.';
UPDATE socrbase SET item_weight = 120 WHERE scname = 'с' OR scname = 'с.' OR scname = 'п' OR scname = 'п.';
UPDATE socrbase SET item_weight = 110 WHERE scname = 'д';
UPDATE socrbase SET item_weight = 100 WHERE scname = 'аул' OR scname = 'аал';
UPDATE socrbase SET item_weight = 90 WHERE scname = 'ул' OR scname = 'ул.';
UPDATE socrbase SET item_weight = 80 WHERE scname = 'пл' OR scname = 'пл.'
                                          OR scname = 'парк' OR scname = 'наб' OR scname = 'наб.'
                                          OR scname = 'пр-кт';

CREATE UNIQUE INDEX kod_t_st_idx
  ON public.socrbase
  USING btree
  (kod_t_st COLLATE pg_catalog."default");

CREATE INDEX scname_level_idx
  ON public.socrbase
  USING btree
  (scname COLLATE pg_catalog."default", level);

CREATE INDEX socrbase_level_fbee6c4a_idx
  ON public.socrbase
  USING btree
  (level, scname COLLATE pg_catalog."default");

CREATE TABLE addrobindex
(
  id serial NOT NULL,
  aoguid uuid NOT NULL,
  parentguid uuid DEFAULT NULL,
  regioncode character varying(2),
  code character varying(17),
  item_fullname text NOT NULL,
  aolevel smallint NOT NULL,
  scname text NOT NULL,
  fullname text NOT NULL,
  item_weight smallint NOT NULL,
  search_vector tsvector,
  CONSTRAINT addrobindex_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

TRUNCATE TABLE addrobindex RESTART IDENTITY;

WITH RECURSIVE PATH (aoguid, parentguid, regioncode, code, aolevel, item_fullname, scname, fullname, item_weight) AS (
  SELECT
    ao.aoguid,
    ao.parentguid,
    ao.regioncode,
    ao.code,
    ao.aolevel,
    ao.formalname as item_fullname,
    sn.socrname::TEXT AS scname,
    ao.shortname || ' ' || ao.formalname AS fullname,
    sn.item_weight
  FROM
    addrob AS ao INNER JOIN socrbase AS sn
      ON (sn.scname = ao.shortname AND sn.level = ao.aolevel)
  WHERE
    ao.aolevel = 1 AND ao.livestatus = 1
  UNION
  SELECT
    child.aoguid, child.parentguid, child.regioncode, child.code, child.aolevel,
    child.formalname as item_fullname,
    PATH.scname::TEXT || ', ' || sn.socrname::TEXT AS scname,
    PATH.fullname || ', ' || child.shortname || ' ' || child.formalname AS fullname,
    sn.item_weight
  FROM
    addrob AS child INNER JOIN socrbase AS sn
      ON (sn.scname = child.shortname AND sn.level = child.aolevel)
    , PATH
  WHERE child.parentguid = PATH.aoguid AND child.livestatus = 1
)
INSERT INTO addrobindex (
  aoguid, parentguid, regioncode, code, aolevel, item_fullname, scname, fullname,
  item_weight, search_vector)
SELECT
  aoguid, parentguid, regioncode, code, aolevel, item_fullname, scname, fullname, item_weight,
  to_tsvector('russian', fullname)
FROM PATH;

DROP INDEX IF EXISTS addrobindex_idx;
CREATE INDEX addrobindex_idx ON addrobindex USING GIN (search_vector);

CREATE INDEX addrobindex_aolevel
  ON public.addrobindex
  USING btree
  (aolevel);

CREATE INDEX addrobindex_item_weight
  ON public.addrobindex
  USING btree
  (item_weight);

CREATE INDEX addrobindex_parentguid
  ON public.addrobindex
  USING btree
  (parentguid);

CREATE INDEX addrobindex_code
  ON public.addrobindex
  USING btree
  (code);

CREATE INDEX addrobindex_region
  ON public.addrobindex
  USING btree
  (regioncode);

DROP TABLE IF EXISTS addrobindex_current;
ALTER TABLE addrobindex RENAME TO addrobindex_current;

ALTER TABLE addrobindex_current RENAME CONSTRAINT addrobindex_pkey TO addrobindexcurrent_pkey;

ALTER SEQUENCE addrobindex_id_seq RENAME TO addrobindexcurrent_id_seq;

ALTER INDEX addrobindex_idx RENAME TO addrobindexcurrent_idx;
ALTER INDEX addrobindex_aolevel RENAME TO addrobindexcurrent_aolevel;
ALTER INDEX addrobindex_item_weight RENAME TO addrobindexcurrent_item_weight;
ALTER INDEX addrobindex_parentguid RENAME TO addrobindexcurrent_parentguid;
ALTER INDEX addrobindex_code RENAME TO addrobindexcurrent_code;
ALTER INDEX addrobindex_region RENAME TO addrobindexcurrent_region;

GRANT SELECT ON TABLE public.addrobindex_current TO fias_user;

COMMIT;
