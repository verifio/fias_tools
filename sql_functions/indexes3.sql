CREATE TABLE public.fias_addrobjindex
(
  id integer NOT NULL DEFAULT nextval('fias_addrobjindex_id_seq'::regclass),
  aoguid uuid NOT NULL,
  aolevel smallint NOT NULL,
  scname text NOT NULL,
  fullname text NOT NULL,
  item_weight smallint NOT NULL,
  search_vector tsvector,
  CONSTRAINT fias_addrobjindex_pkey PRIMARY KEY (id),
  CONSTRAINT fias_addrobjindex_aolevel_check CHECK (aolevel >= 0),
  CONSTRAINT fias_addrobjindex_item_weight_check CHECK (item_weight >= 0)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.fias_addrobjindex
  OWNER TO postgres;

-- Index: public.fias_addrobjindex_aolevel_e8a04882_uniq

-- DROP INDEX public.fias_addrobjindex_aolevel_e8a04882_uniq;

CREATE INDEX fias_addrobjindex_aolevel_e8a04882_uniq
  ON public.fias_addrobjindex
  USING btree
  (aolevel);

-- Index: public.fias_addrobjindex_item_weight_96602e94_uniq

-- DROP INDEX public.fias_addrobjindex_item_weight_96602e94_uniq;

CREATE INDEX fias_addrobjindex_item_weight_96602e94_uniq
  ON public.fias_addrobjindex
  USING btree
  (item_weight);


-- ===================


CREATE TABLE public.fias_addrobj
(
  ifnsfl character varying(4),
  terrifnsfl character varying(4),
  ifnsul character varying(4),
  terrifnsul character varying(4),
  okato character varying(11),
  oktmo character varying(11),
  postalcode character varying(6),
  updatedate date NOT NULL,
  startdate date NOT NULL,
  enddate date NOT NULL,
  normdoc uuid,
  aoguid uuid NOT NULL,
  parentguid uuid,
  aoid uuid NOT NULL,
  previd uuid,
  nextid uuid,
  formalname character varying(120) NOT NULL,
  offname character varying(120),
  shortname character varying(10) NOT NULL,
  aolevel smallint NOT NULL,
  regioncode character varying(2) NOT NULL,
  autocode character varying(1) NOT NULL,
  areacode character varying(3) NOT NULL,
  citycode character varying(3) NOT NULL,
  ctarcode character varying(3) NOT NULL,
  placecode character varying(3) NOT NULL,
  streetcode character varying(4) NOT NULL,
  extrcode character varying(4) NOT NULL,
  sextcode character varying(3) NOT NULL,
  code character varying(17),
  plaincode character varying(15),
  actstatus boolean NOT NULL,
  centstatus_id integer NOT NULL,
  operstatus_id integer NOT NULL,
  currstatus_id integer NOT NULL,
  livestatus boolean NOT NULL,
  cadnum character varying(100),
  divtype character varying(1) NOT NULL,
  plancode character varying(4) NOT NULL,
  CONSTRAINT fias_addrobj_pkey PRIMARY KEY (aoguid),
  CONSTRAINT fias_addrobj_centstatus_id_b39cccfa_fk_fias_centerst_centerstid FOREIGN KEY (centstatus_id)
      REFERENCES public.fias_centerst (centerstid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT fias_addrobj_currstatus_id_bd50ef0d_fk_fias_curentst_curentstid FOREIGN KEY (currstatus_id)
      REFERENCES public.fias_curentst (curentstid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT fias_addrobj_operstatus_id_4275745e_fk_fias_operstat_operstatid FOREIGN KEY (operstatus_id)
      REFERENCES public.fias_operstat (operstatid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT fias_addrobj_aoid_key UNIQUE (aoid),
  CONSTRAINT fias_addrobj_aolevel_check CHECK (aolevel >= 0)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.fias_addrobj
  OWNER TO postgres;

-- Index: public.fias_addrobj_69e75dd2

-- DROP INDEX public.fias_addrobj_69e75dd2;

CREATE INDEX fias_addrobj_69e75dd2
  ON public.fias_addrobj
  USING btree
  (aolevel);

-- Index: public.fias_addrobj_6a7323e5

-- DROP INDEX public.fias_addrobj_6a7323e5;

CREATE INDEX fias_addrobj_6a7323e5
  ON public.fias_addrobj
  USING btree
  (parentguid);

-- Index: public.fias_addrobj_aolevel_f2a98edf_idx

-- DROP INDEX public.fias_addrobj_aolevel_f2a98edf_idx;

CREATE INDEX fias_addrobj_aolevel_f2a98edf_idx
  ON public.fias_addrobj
  USING btree
  (aolevel, shortname COLLATE pg_catalog."default");

-- Index: public.fias_addrobj_centstatus_id_b39cccfa_uniq

-- DROP INDEX public.fias_addrobj_centstatus_id_b39cccfa_uniq;

CREATE INDEX fias_addrobj_centstatus_id_b39cccfa_uniq
  ON public.fias_addrobj
  USING btree
  (centstatus_id);

-- Index: public.fias_addrobj_currstatus_id_bd50ef0d_uniq

-- DROP INDEX public.fias_addrobj_currstatus_id_bd50ef0d_uniq;

CREATE INDEX fias_addrobj_currstatus_id_bd50ef0d_uniq
  ON public.fias_addrobj
  USING btree
  (currstatus_id);

-- Index: public.fias_addrobj_d587a1b5

-- DROP INDEX public.fias_addrobj_d587a1b5;

CREATE INDEX fias_addrobj_d587a1b5
  ON public.fias_addrobj
  USING btree
  (shortname COLLATE pg_catalog."default");

-- Index: public.fias_addrobj_d906fd80

-- DROP INDEX public.fias_addrobj_d906fd80;

CREATE INDEX fias_addrobj_d906fd80
  ON public.fias_addrobj
  USING btree
  (formalname COLLATE pg_catalog."default");

-- Index: public.fias_addrobj_formalname_ef614a2e_like

-- DROP INDEX public.fias_addrobj_formalname_ef614a2e_like;

CREATE INDEX fias_addrobj_formalname_ef614a2e_like
  ON public.fias_addrobj
  USING btree
  (formalname COLLATE pg_catalog."default" varchar_pattern_ops);

-- Index: public.fias_addrobj_operstatus_id_4275745e_uniq

-- DROP INDEX public.fias_addrobj_operstatus_id_4275745e_uniq;

CREATE INDEX fias_addrobj_operstatus_id_4275745e_uniq
  ON public.fias_addrobj
  USING btree
  (operstatus_id);

-- Index: public.fias_addrobj_shortname_8ba2639f_like

-- DROP INDEX public.fias_addrobj_shortname_8ba2639f_like;

CREATE INDEX fias_addrobj_shortname_8ba2639f_like
  ON public.fias_addrobj
  USING btree
  (shortname COLLATE pg_catalog."default" varchar_pattern_ops);

-- Index: public.fias_addrobj_shortname_a36755d0_idx

-- DROP INDEX public.fias_addrobj_shortname_a36755d0_idx;

CREATE INDEX fias_addrobj_shortname_a36755d0_idx
  ON public.fias_addrobj
  USING btree
  (shortname COLLATE pg_catalog."default", formalname COLLATE pg_catalog."default");


-- ===============


CREATE TABLE public.fias_socrbase
(
  level smallint NOT NULL,
  scname character varying(10) NOT NULL,
  socrname character varying(50) NOT NULL,
  kod_t_st integer NOT NULL,
  item_weight smallint NOT NULL,
  CONSTRAINT fias_socrbase_pkey PRIMARY KEY (kod_t_st),
  CONSTRAINT fias_socrbase_item_weight_check CHECK (item_weight >= 0),
  CONSTRAINT fias_socrbase_kod_t_st_check CHECK (kod_t_st >= 0),
  CONSTRAINT fias_socrbase_level_check CHECK (level >= 0)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.fias_socrbase
  OWNER TO postgres;

-- Index: public.fias_socrbase_level_fbee6c4a_idx

-- DROP INDEX public.fias_socrbase_level_fbee6c4a_idx;

CREATE INDEX fias_socrbase_level_fbee6c4a_idx
  ON public.fias_socrbase
  USING btree
  (level, scname COLLATE pg_catalog."default");