#!/usr/bin/env python

convert_sql = """
BEGIN;
    alter table {table} rename column {column} to {column}_x;
    alter table {table} add column {column} {cast};
    update {table} set {column} = {column}_x::{cast};
    alter table {table} drop column {column}_x;
COMMIT;
"""

def convert(table, column, cast):
    return convert_sql.format(table=table, column=column, cast=cast)

def null(table, column):
    return "UPDATE {table} SET {column} = NULL WHERE {column} = '';".format(table=table, column=column)

if __name__=='__main__':
    import sys, json

    spec = json.load(open(sys.argv[1]))
    for table, task in spec.iteritems():
        for column in task.get('null', []):
            print null(table, column)
        for column, cast in sorted(task.get('convert', {}).iteritems()):
            print convert(table, column, cast)
