TRUNCATE TABLE addrobindex RESTART IDENTITY;


WITH RECURSIVE PATH (aoguid, parentguid, regioncode, aolevel, scname, fullname, item_weight) AS (
  SELECT
    ao.aoguid,
    ao.parentguid,
    ao.regioncode,
    ao.aolevel,
    sn.socrname::TEXT AS scname,
    ao.shortname || ' ' || ao.formalname AS fullname,
    sn.item_weight
  FROM
    addrob AS ao INNER JOIN socrbase AS sn
      ON (sn.scname = ao.shortname AND sn.level = ao.aolevel)
  WHERE
    ao.aolevel = 1 AND ao.livestatus = 1
  UNION
  SELECT
    child.aoguid, child.parentguid, child.regioncode, child.aolevel,
    PATH.scname::TEXT || ', ' || sn.socrname::TEXT AS scname,
    PATH.fullname || ', ' || child.shortname || ' ' || child.formalname AS fullname,
    sn.item_weight
  FROM
    addrob AS child INNER JOIN socrbase AS sn
      ON (sn.scname = child.shortname AND sn.level = child.aolevel)
    , PATH
  WHERE child.parentguid = PATH.aoguid AND child.livestatus = 1
)
INSERT INTO addrobindex (
  aoguid, parentguid, regioncode, aolevel, scname, fullname, 
  item_weight, search_vector)
SELECT 
  aoguid, parentguid, regioncode, aolevel, scname, fullname, item_weight, 
  to_tsvector('russian', fullname) 
FROM PATH;


WITH RECURSIVE PATH (aoguid, aolevel, scname, fullname, item_weight) AS (
  SELECT
    ao.aoguid,
    ao.aolevel,
    sn.socrname::TEXT AS scname,
    ao.shortname || ' ' || ao.formalname AS fullname,
    sn.item_weight
  FROM
    addrob AS ao INNER JOIN socrbase AS sn
      ON (sn.scname = ao.shortname AND sn.level = ao.aolevel)
  WHERE
    ao.aolevel = 1 AND ao.livestatus = 1
  UNION
  SELECT
    child.aoguid, child.aolevel,
    PATH.scname::TEXT || ', ' || sn.socrname::TEXT AS scname,
    PATH.fullname || ', ' || child.shortname || ' ' || child.formalname AS fullname,
    sn.item_weight
  FROM
    addrob AS child INNER JOIN socrbase AS sn
      ON (sn.scname = child.shortname AND sn.level = child.aolevel)
    , PATH
  WHERE child.parentguid = PATH.aoguid AND child.livestatus = 1
)
SELECT 
  aoguid, aolevel, scname, fullname, item_weight, 
  to_tsvector('russian', fullname) 
FROM PATH LIMIT 1;
    


DROP INDEX IF EXISTS addrobindex_idx;
CREATE INDEX addrobindex_idx ON addrobindex USING GIN (search_vector);
