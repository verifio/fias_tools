# FIAS Tools

*`sql_functions` directory is for experiments only*

## Prerequisites
### Aria2
https://aria2.github.io/

### PgDBF
install pgdbf from 'patch-1' branch: https://github.com/justintocci/pgdbf/tree/patch-1

```
git clone git@github.com:justintocci/pgdbf.git
git checkout patch-1
./configure
autoreconf -vfi
./configure
make
sudo make install
```

## Init
run `init.sh` to create database

## Update
run `update.sh` to download, extract and populate database

use `nohup ./update.sh &` to run as background task

## Examples of usage:
```sql
SELECT * FROM addrobindex_current WHERE  search_vector @@ to_tsquery('russian', 'ульяновск & 2 & мира') ORDER BY item_weight DESC

SELECT * FROM addrobindex_current WHERE  search_vector @@ to_tsquery('russian', 'ульяновск & ленинского <-> комсомола') ORDER BY item_weight DESC

SELECT * FROM addrobindex_current WHERE  search_vector @@ to_tsquery('russian', 'г. <-> ульяновск') ORDER BY item_weight DESC
```