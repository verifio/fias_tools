#!/bin/bash
PATHTO=/srv/fias
TOOLS=$PATHTO/fias_tools
LOG=$PATHTO/init_db.log

echo "" >> $LOG
echo "========================= $(date '+%F %T') INIT START =========================" >> $LOG

export PGPASSWORD="postgres"

SQL="
DROP DATABASE fias;
CREATE DATABASE fias \
  WITH OWNER = postgres \
       ENCODING = 'UTF8' \
       TABLESPACE = pg_default \
       LC_COLLATE = 'ru_RU.UTF-8' \
       LC_CTYPE = 'ru_RU.UTF-8'
	TEMPLATE template0;
GRANT CONNECT, TEMPORARY ON DATABASE fias TO public; \
GRANT ALL ON DATABASE fias TO postgres;"

echo $SQL | psql -U postgres >> $LOG 2>&1

PGPASSWORD=""
echo "$(date '+%F %T') INIT DONE!" | tee -a $LOG
